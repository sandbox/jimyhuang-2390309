<?php

/*
 * Implement hook_menu();
 */

function static_html_wget_rsync_menu() {
  $items['admin/config/static_html_wget/setting/rsync'] = array(
    'title' => 'rsync Settings',
    'description' => 'Settings of FTP uploader for static_html_wget.',
    'position' => 'right',
    'weight' => -5,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('static_html_wget_rsync_setting_form'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_LOCAL_TASK,
  );
  return $items;
}

/*
 * Implement hook_form();
 * 
 * ToDo: Add ssh setting param: StrictHostKeyChecking=(no, ask, yes);
 */

function static_html_wget_rsync_setting_form($form, &$form_state) {
  $initial = true;
  $useSSHkey = false;
  $sync_settings = json_decode(variable_get('static_sites_rsync_settings', ''), true);
  if ($sync_settings) {
    $initial = false;
//    $useSSHkey = isset($sync_settings['sshkey']);
  }
//  ctools_include('dependent');
  $sshpass_exist = true;
  if (!shell_command_exist('sshpass') && $initial) {
    drupal_set_message(t('sshpass is not installed in your server. <br />You got to make sure key pairs between local and remote servers is ready , or the rsync will not work!'), 'warning');
    $sshpass_exist = false;
  }



  $domains = _get_active_domains();
//  $domains[] = 'staging.uclab.jp';
  $form['static_sites_rsync_settings'] = array(
    '#markup' => 'Settings for rsync (ssh).',
  );

  $domain_counter = 0;
  foreach ($domains as $domain) {

    $form['static_sites_rsync_' . $domain_counter] = array(
      '#type' => 'fieldset',
      '#title' => $domain,
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
      '#attributes' => array('class' => array('subdomain-settings')),
    );

    $form['static_sites_rsync_' . $domain_counter]['host-' . $domain_counter] = array(
      '#title' => t('Destination Host'),
      '#type' => 'textfield',
      '#size' => 20,
      '#maxlength' => 128,
      '#default_value' => (($initial) ? '' : $sync_settings[$domain]['host']),
      '#description' => t('Enter IP or hostname. 123.123.123.123, example.com, ...etc.'),
    );
    $form['static_sites_rsync_' . $domain_counter]['port-' . $domain_counter] = array(
      '#title' => t('Port'),
      '#type' => 'textfield',
      '#size' => 5,
      '#maxlength' => 5,
      '#default_value' => (($initial) ? 22 : $sync_settings[$domain]['port']),
      '#element_validate' => array('element_validate_integer_positive'),      
    );
    $form['static_sites_rsync_' . $domain_counter]['username-' . $domain_counter] = array(
        '#title' => t('Username'),
        '#type' => 'textfield',
        '#size' => 20,
        '#maxlength' => 128,
        '#default_value' => ($initial) ? '' : $sync_settings[$domain]['username'],
      );
    if ($sshpass_exist) {      
      $form['static_sites_rsync_' . $domain_counter]['password-' . $domain_counter] = array(
        '#title' => t('Password'),
        '#type' => 'password',
        '#size' => 20,
        '#maxlength' => 64,
        '#description' => t('If you have already entered your password before, you should leave this field blank, unless you want to change the stored password.'),
      );
    }
    $form['static_sites_rsync_' . $domain_counter]['dest_dir-' . $domain_counter] = array(
      '#title' => t('Destination directory'),
      '#type' => 'textfield',
      '#size' => 20,
      '#maxlength' => 128,
      '#default_value' => ($initial) ? '' : $sync_settings[$domain]['dest_dir'],
      '#description' => t('Enter the directory (path) where to put upload files on the FTP serve. Do not include preceding or trailing slashes.'),      
    );
    $form['static_sites_rsync_' . $domain_counter]['sshkey-' . $domain_counter] = array(
      '#type' => 'checkbox',
      '#title' => t('Connet with SSH key'),
      '#default_value' => ($initial) ? 0 : $sync_settings[$domain]['sshkey'],
      '#description' => t('If checked, rsync will connect with key-pairs. Please make sure the ssh keys is  ready.'),
    );
    $domain_counter++;
  }

  $form['domains'] = array('#type' => 'hidden', '#value' => $domains);
  $form['sshpass'] = array('#type' => 'hidden', '#value' => $sshpass_exist);
  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));

  return $form;
}

function static_html_wget_rsync_setting_form_validate($form, &$form_state) {
  $domain_counter = 0;  
  foreach ($form_state['values']['domains'] as $domain) {    
    $host = check_plain($form_state['values']['host-' . $domain_counter]);
    if(!$host){$domain_counter++;continue;}
    $port = $form_state['values']['port-' . $domain_counter];
    $uname = $form_state['values']['username-' . $domain_counter];
    $dest_dir=$form_state['values']['dest_dir-' . $domain_counter];
    if($host && !$uname){form_set_error('username-' . $domain_counter, t('Username is required.'));}
    if($host && !$dest_dir){form_set_error('dest_dir-' . $domain_counter, t('Destination directory is required.'));}
    if($form_state['values']['sshkey-'. $domain_counter]){$domain_counter++;continue;}
    $conn_check = static_html_wget_rsync_connect_check($host, $port);
    switch ($conn_check) {
      case 'host':
        form_set_error('host-' . $domain_counter, t('Connection failed. Check to make sure the FTP host is correct.'));
        break;
      case 'nossh2':
        form_set_error('', t('Cannot find <em>ssh2_connect</em> function. Please install first.'));
        break;
    }
    $domain_counter++;
  }
}

function static_html_wget_rsync_setting_form_submit($form, &$form_state) {
  $sync_ori_settings = json_decode(variable_get('static_sites_rsync_settings', ''), true);
  $sync_new_settings = array();
  $domain_counter = 0;
  foreach ($form_state['values']['domains'] as $domain) {
    $sync_new_settings[$domain]['host'] = trim(check_plain($form_state['values']['host-' . $domain_counter]));
    $sync_new_settings[$domain]['port'] = trim(check_plain($form_state['values']['port-' . $domain_counter]));
    $sync_new_settings[$domain]['username'] = trim(check_plain($form_state['values']['username-' . $domain_counter]));
    $upass = trim(check_plain($form_state['values']['password-' . $domain_counter]));
    if ($upass) {
      $sync_new_settings[$domain]['password'] = $upass;
    }
    else {
      $sync_new_settings[$domain]['password'] = $sync_ori_settings[$domain]['password'];
    }
    $sync_new_settings[$domain]['dest_dir'] = trim(check_plain($form_state['values']['dest_dir-' . $domain_counter]));
    $sync_new_settings[$domain]['sshkey']=$form_state['values']['sshkey-'. $domain_counter];
    $domain_counter++;
  }
  $sync_settings = json_encode($sync_new_settings);
  variable_set('static_sites_rsync_settings', $sync_settings);
  $form_state['rebuild'] = true;
}

/**
 * Implements hook_block_info().
 * 
 */
function static_html_wget_rsync_block_info() {
  $blocks = array();
  $blocks['rsync_synchonize_form'] = array(
    'info' => t('Execute Synchonize Form'),
//    'status' => TRUE,
  );
  return $blocks;
}

/**
 * Implements hook_block_view().
 * 
 */
function static_html_wget_rsync_block_view($delta = '') {

  //The $delta parameter tells us which block is being requested.
  $block = array();
  switch ($delta) {
    case 'rsync_synchonize_form':
      if (user_access('administer wget static html')) {
        $block['subject'] = t('Execute Synchonize Form');
//            $block['content'] = drupal_get_form('static_html_wget_rsync_exe_form');
        $block['content'] = drupal_get_form('rsync_exe_form');
        break;
      }
  }
  return $block;
}

function rsync_exe_form($form, &$form_state) {
  $sync_settings = json_decode(variable_get('static_sites_rsync_settings', ''), true);
  $disable_form=false;
  global $base_url;
  $current_host = str_replace('http://', '', $base_url);
  $form = array();
  if(isset($sync_settings[$current_host]['host']) && !$sync_settings[$current_host]['host']){
    $disable_form=true;
    $form['info'] = array('#markup' => t('You have to setup rsync related <a href="!url">settings</a> first', array('!url' => '/admin/config/static_html_wget/setting/rsync')));
  }
  $form['current_domain'] = array('#type' => 'hidden', '#value' => $current_host);
  $form['submit'] = array('#type' => 'submit', '#value' => t('Synchronize'),'#disabled'=>$disable_form);
  return $form;
}

function rsync_exe_form_submit($form, &$form_state) {
  $sync_settings = json_decode(variable_get('static_sites_rsync_settings', ''), true);
  if (!$sync_settings) {
    drupal_set_message(t('You have to setup rsync related <a href="!url">settings</a> first', array('!url' => '/admin/config/static_html_wget/setting/rsync')), 'warning');
  }
  else {
    $output = wget_static_html($form_state['values']['current_domain']);
    $cmd = _prepare_rsync_cmd($form_state['values']['current_domain']);
//    $cmd2 = _prepare_lftp_cmd($form_state['values']['current_domain']);
//    dpm($cmd);
    $output.= '<br />'.t('Synchonized data:').'<br />&nbsp;&nbsp;'.shell_exec($cmd);
    drupal_set_message($output);
  }
}

//
/**
 * 
 * @param type $remote_host
 * @param type $port
 * @param type $username
 * @param type $password
 * @param type $remote_path
 */
function static_html_wget_rsync_connect_check($remote_host, $port=22, $username = '', $password = '', $remote_path = '') {
  if(!function_exists('ssh2_connect')){
    return 'nossh2';
  }
  $conn = ssh2_connect($remote_host, $port);
  if (!$conn) {
    return 'host';
  }
//  if (!$username || !$password) {
//    return 'host ok';
//  }
//  $auth = ssh2_auth_password($conn, $username, $password);
//  if (!$auth) {
//    return 'username][password';
//  }
//  $sftp = ssh2_sftp($conn);
//  $dirExists = is_dir('ssh2.sftp://' . $sftp . $remote_path);
//  if (!$dirExists) {
//    return 'directory';
//  }
  return 'ok';
}

function _prepare_rsync_cmd($current_domain, $sshpass = true) {
  $local_dest = variable_get('static_obj_site_' . $current_domain);
  $real_path = filter_input(INPUT_SERVER, 'DOCUMENT_ROOT', FILTER_SANITIZE_URL) . '/' . $local_dest;
  if (!$local_dest) {
    drupal_set_message(t('You should setup the source directory <a href="!url">here</a> first', array('!url' => '/admin/config/static_html_wget/setting/fetch')), 'warning');
    return;
  }
  $sync_settings = json_decode(variable_get('static_sites_rsync_settings', ''), true);
//  dpm($sync_settings);
  $rsync_settings = $sync_settings[$current_domain];
  if (!$rsync_settings['sshkey']) {
    $cmd = sprintf('rsync -e "sshpass -p %s ssh -p %d -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null" --progress -az %s/* %s@%s:%s |grep total', $rsync_settings['password'], $rsync_settings['port'], $real_path, $rsync_settings['username'], $rsync_settings['host'], $rsync_settings['dest_dir']);
  }else {
    $cmd = sprintf('rsync -az --progress -e "ssh -p %d %s/*" %s@%s:%s |grep total', $rsync_settings['port'], $real_path,$rsync_settings['username'], $rsync_settings['host'], $rsync_settings['dest_dir']);
  }
  return $cmd;
}

function _prepare_lftp_cmd($current_domain) {
  $local_dest = variable_get('static_obj_site_' . $current_domain);
  $real_path = filter_input(INPUT_SERVER, 'DOCUMENT_ROOT', FILTER_SANITIZE_URL) . '/' . $local_dest;
  if (!$local_dest) {
    drupal_set_message(t('You should setup the source directory <a href="!url">here</a> first', array('!url' => '/admin/config/static_html_wget/setting/fetch')), 'warning');
    return;
  }
  $sync_settings = json_decode(variable_get('static_sites_rsync_settings', ''), true);
//  dpm($sync_settings);
  $rsync_settings = $sync_settings[$current_domain];
  $cmd = sprintf('lftp -c "open -u %s,%s sftp://%s:%d ; mirror -R %s %s"', $rsync_settings['username'], $rsync_settings['password'], $rsync_settings['host'], $rsync_settings['port'], $real_path, $rsync_settings['dest_dir']);
  return $cmd;
}
